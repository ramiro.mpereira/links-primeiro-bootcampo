1 - Intro: https://www.youtube.com/playlist?list=PLuTtxbP6vKpJSsh9Cwm2fNvOxHaMqElYV

2 - Git: https://youtu.be/u3gIlXkm5D0?list=PLntw1rfK2xiVIle_sbE40Vz126yNMVUFw

3 - Mobile (Ionic): https://youtube.com/playlist?list=PLt_YrqP7GVMFwtiteQwmcBDxKCKf8CEdS

4 - API (Lumen): https://youtu.be/3VONXEIUogA

5 - Retaguarda (Angular): em produção

6 - Infra (Google Cloud): https://youtube.com/playlist?list=PLuTtxbP6vKpK_cNXmd_kAFYXMK23dc1-X / Anotações disponíveis em: https://bootcamp-309619.rj.r.appspot.com/

7 - Trilha individual: https://www.youtube.com/playlist?list=PLuTtxbP6vKpL2kwX45cPdoteMebpxWuU3 
